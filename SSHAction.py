import time
import logging
import subprocess

from BaseAction import BaseAction
from Config import config

# needs passwordless ssh, and currently only supporting parallel-ssh

class SSHAction(BaseAction):
    def __init__(self):
        self.nodes = config.get('ssh','nodes')  # for testing, or xy[01-12] expression covering all nodes
        self.debug = config.getboolean('ssh','debug')
        super().__init__()
        self.reduceCmd = config.get('ssh','reduce')
        self.resumeCmd = config.get('ssh','resume')

    def reducePower(self,start,duration):
        cmd = self.basecmd + ['parallel-ssh','-H',self.nodes,self.reduceCmd]
        sc = subprocess.run(cmd,stdout=subprocess.PIPE,
                         universal_newlines=True)
        logging.info(sc)

    def resume(self):
        cmd = self.basecmd + ['parallel-ssh','-H',self.nodes,self.resumeCmd]
        sc = subprocess.run(cmd,stdout=subprocess.PIPE,
                         universal_newlines=True)
        logging.info(sc)

if __name__ == '__main__':
    import sys
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    a = SSHAction()
    a.reducePower()
    time.sleep(5)
    a.resume()