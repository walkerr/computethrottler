# ComputeThrottler

Service to schedule periods of reduced power consumption, using various external sources. Includes actions to reduce power via Batch System, CPU frequency modulation, or other.


## Installation
No rpm or much idea yet.
Clone somewhere, maybe /usr/local/lib/python3.6/site-packages. Need python3 and ENTSOE has dependencies on panda, arrow, request - not needed if comment out import and test with Awattar. Included systemd compute-throttler.service to copy to /usr/lib/systemd/system/compute-throttler.service (on CentOS7 at least)
systemctl enable systemctl enable

## Configuration
In "global" section choose forecast source and the powersaving action. The duration of most sleep actions and reduced by a factor of the "speedup" value, for testing purposes. 
Action section have "nodes" value to set the list of hosts on which to do the reduce action. For "slurm" it is the standard format, e.g. with [01-12] and commas. 

## Usage
python3 ComputeThrottler.py 
or
systemctl start compute-throttler.service

## Forcast sources
- Awattar is only available for germany and Austria but has the advantage of needing no access token, so good for initial testing.
- ENTSOE Transparency platform
To use the ENTSOE api, for European data, you`ll need a token. You need to create and account and request one on
https://transparency.entsoe.eu/dashboard/show
Needs python numpy, beautifulsoup4, arrow, requests pandas
Could not find rpm for pandas so, python3 -m pip install pandas
- UK National Grid ESO carbon intensity https://data.nationalgrideso.com/carbon-intensity1/national-carbon-intensity-forecast
- Signal with start time and duration from Grid operator or energy provider. Supports mail to IMAP server. Also stdin for test.


## Actions to reduce power consumption
- Slurm suspend
- parallel-ssh to list of nodes with configurable command to reduce and resume, e.g. cpupower frequency-set -g powersave
- update file on sharedFS to be used by cron on WNs to make switches. Example in scripts dir.

## Scheduling
- Initial algorithm for 2 1hr pauses per day, clearly with optimization potential.
Find the next maximum that is 10% over the average value and get start time. 
1. Sleep until this start time, then do reduce action
2. Sleep for 1hr then do resume action.
3. Sleep for 3hrs to avoid another pause in same peak.
- Choose period, with a maximum length, over threshold with sliding window to maximize saving.

## Contributing
Very open to contributions including complete re-write.

## Project status
Usable demo stage.

