import os
import configparser
import logging

# Need to locate the config file
clocations = ["/etc/computethrottler/computethrottler.conf","./computethrottler.conf"]
for conffile in clocations:
    if os.path.exists(conffile): break

#conffile = "computethrottler.conf"

print ("Using config file: " + conffile)

config = configparser.ConfigParser()
config.read(conffile)
#print ( config.sections())
