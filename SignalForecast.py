

# Signal from National Grid, Provider etc with specific starttime and duration
# Email of specific format but also stdin/file for test purposes

import time,sys
from BaseForecast import BaseForecast
from BaseSignaller import BaseSignaller
from ImapSignaller import ImapSignaller
from Config import config


class SignalForecast(BaseForecast):
    def __init__(self):
        super().__init__(self)
        self.source = config.get("signal","source")

    def getForecast(self):
# to get the forecast (step function) we need to wait for the signal
# rather than fill a step function then analysis, may as well just set start and duration from the signal
      if self.source == 'stdin':
          startstr = input("Enter start time (HH:MM)?") or time.strftime("%H:%M")
          self.start = time.strptime(startstr,"%H:%M")
          self.duration = 60
      elif self.source == 'imap':
          sig = ImapSignaller()
      elif self.source == 'gmail':
          sig = GmailSignaller()
# now we wait

      if isinstance(sig,BaseSignaller): self.start,self.duration = sig.listen()

    def nextPause(self):
        return self.start,self.duration

if __name__ == '__main__':
    f = SignalForecast()

    f.getForecast()
    f.nextPause()