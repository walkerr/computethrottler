import time
import logging
import ENTSOE
from BaseForecast import BaseForecast
from Config import config


class EntsoeForecast(BaseForecast):
    def __init__(self):
        super().__init__(self)
        self.region = config.get("entsoe","region")
        self.forecast = None

    def getForecast(self):
    # returns an overlapping mix of 15min and 1hrs periods.
    # will discard the 15min ones
        l = ENTSOE.fetch_price(self.region)
        #print (l)
        timevals = []
       # add the endtime using the starttime from the following entry
       # final entry should get an entime using the same period
        last = {}
        for d in l:
            start = d['datetime'].timestamp()
            this = {'start': start, 'value':d['price']}
            if last:
               last['end'] = start - 1
               period = start-last['start'] - 1
            # discard 15min periods
               if period > 1800:
                timevals.append(last)
                logging.debug(time.ctime(last['start']),last['value'])

            last = this
    # now add the last entry with best guess end time
        last['end'] = last['start'] + period
        timevals.append(last)
        logging.info("Got a time series of length: %d"%len(timevals))
        self.forecast = timevals

if __name__ == '__main__':
    f = EntsoeForecast()
    f.getForecast()

    print(f.forecast)
    #f.scheduleNextAction()
    f.nextMax()