# Base class to get forecast of some value
import urllib.parse
import time
import logging
from Config import config

class BaseForecast:
    def __init__(self, url, region='DE'):
        #self.logger = logging.getLogger('Forecast')
        #self.logger.addHandler(logging.FileHandler(filename='myapp.log'))
        self.region = region
        self.forecast = None
        self.maxPause = config.getint("ukeso","maxPause")

        #self.url = url
        #self.uhost, self.upath = self.splitUrl()

    def getForecast(self):
        # TBD remove periods already passed.
        pass

    def splitUrl(self):
        pu = urllib.parse.urlparse(self.url)
        #print(pu.scheme, pu.netloc, pu.path)
        return pu.netloc, pu.path

    def scheduleNextAction(self):
        # find start and duration of next contiguous above limit period
        pstart = 0
        pduration = 0
        windows = [] # sliding window of length self maxPause to store total of values
        for d in self.forecast:
            # print (d)
            if d['value'] >= self.threshold:
                if pstart == 0:
                    pstart = d['start']
                windows.append({'start':d['start'], 'total': d['value']})
                for window in windows:
                    if d['start'] - window['start'] < self.maxPause: window['total']+= d['value']
                logging.debug("Above threshold %d %d"%(pstart, d['value']))
            else:
                if pstart:
                    logging.debug("Below threshold again %d %f"%(d['end'],d['value']))
                    break
        pduration = int((d['end'] - pstart) / 60)
        logging.debug("Got %d %d mins"%(pstart, pduration))
        if pstart == 0:
            logging.info("Not above threshold: %d"%self.threshold)
        # need a max length of pause and then slide this across longer period to maximize.
        # can do this in loop already window(pstart,total) and choose max total
        if pduration > self.maxPause:
            print (windows)
            logging.debug("Pause is longer then max - maximizing sliding window")
            maxWindow = max(windows, key=lambda x:x['total'])
            pstart = maxWindow['start']
            pduration = self.maxPause
            logging.debug("Using start %d duration %d mins with total %f"%(pstart, pduration,maxWindow['total']))
        return pstart, pduration

    def nextMax(self):
        # next 1hr, 30% above mean, say...
        pabove = 1.1
        vmean = sum(d['value'] for d in self.forecast) / len(self.forecast)
        vmax = max(d['value'] for d in self.forecast)
        localMax = 0
        for d in self.forecast:
           # skip the past
            if d['end'] < time.time() : continue
            dv = d['value']
            if dv < vmean : continue
            if dv >= localMax:
                localMax = dv
                localMaxD = d
            else:
                break

        logging.debug(vmean,vmax, localMax,localMaxD,time.ctime(localMaxD['start']))
        return localMaxD['start'],int(localMaxD['end']-localMaxD['start'])/60

    def fillEnd(self,timevals):
     # Add the end timestamp to a list if dicts with just start time
        last={}
        newtimevals=[]
        for d in timevals:
            #print (d)
            this = d
            if last:
                last['end'] = d['start'] - 1
                period = d['start']-last['start'] - 1
                newtimevals.append(last)
                logging.debug(time.ctime(last['start']))
            last = d
         # now add the last entry with best guess end time
        last['end'] = last['start'] + period
        newtimevals.append(last)
        # return ordered by start
        return sorted(newtimevals, key=lambda d: d['start'])

if __name__ == '__main__':
    f = BaseForecast('https://api.awattar.de/v1/marketdata')
