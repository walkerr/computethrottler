import time
import subprocess
import logging

from BaseAction import BaseAction
from Config import config

class SlurmAction(BaseAction):
    def __init__(self):
        self.user = config.get('slurm','user') # user for reservation, i.e. not submitting jobs!
        self.nodes = config.get('slurm','nodes')  # for testing, or xy[01-12] expression covering all nodes
      # accelerate by factor 60, and only echo actions
        self.debug = config.getboolean('slurm','debug')
        if self.debug:
            self.basecmd = ['echo']
        else:
            self.basecmd = []

    def reducePower(self,start,duration):
        # create reservation and immediately suspend batch jobs.
        # Draining first not helpful. But want to stop any new jobs.
        sstart = time.strftime('%Y-%m-%dT%H:%M')
        sduration = duration # in minutes. Might have to convert.

        cmd = self.basecmd + ['scontrol','create','reservation','user=%s'%self.user]
        cmd+=['starttime=%s'%sstart, 'Duration=%d'%sduration, 'nodes=%s'%self.nodes]
        cmd+=['flags=ignore_jobs', 'Reservation=SaveGas']
        sc = subprocess.run(cmd,stdout=subprocess.PIPE,
                         universal_newlines=True)
        logging.info(sc)
     # list all Running jobs on the configured nodes
        cmd = self.basecmd + ['squeue','-ho','%A','-t','R','-w',self.nodes]
        sq = subprocess.run(cmd,stdout=subprocess.PIPE,
                         universal_newlines=True)
        #print(sq)
        if sq.returncode != 0 :
            print ( sq )
            if self.debug:
                # debug so just set some ids
                jobs=['1111','1112','1113']
            else:
                logging.warning('Abandon reducePower')
                return 1
        else:
           #create jobs list to add to suspend command
            jobs = sq.stdout.splitlines()
       # validate it is a list of integers - TBD
        #if self.debug: jobs=['1111','1112','1113']
        logging.info('Will now suspend %d jobs on %s'%(len(jobs), self.nodes))
        cmd = self.basecmd + ['scontrol','suspend'] + jobs

        sc = subprocess.run(cmd,stdout=subprocess.PIPE,
                         universal_newlines=True)
        #print (sc)

    @property
    def resume(self):
        # ensure reservation is gone and resume batch jobs
        cmd='scontrol show res SaveGas'
        # list all Suspended jobs on the configured nodes
        # NB: will also resume jobs suspended for any other reason
        # could store jobs but prefer to keep stateless. Maybe store with fallback to all.
        cmd = self.basecmd + ['squeue','-ho','%A','-t','S','-w',self.nodes]
        sq = subprocess.run(cmd,stdout=subprocess.PIPE,
                         universal_newlines=True)
        logging.info(sq)
        if sq.returncode != 0:
            #print ( sq )
            logging.warning('Abandon resume' + sq)
            return 1
        logging.info("Removed reservation")
       #create jobs list to add to resume command
        jobs = sq.stdout.splitlines()
       # validate it is a list of integers - TBD
        if self.debug: jobs=['1111','1112','1113']
        logging.info('Will now resume %d jobs on %s'%(len(jobs), self.nodes))
        cmd = self.basecmd + ['scontrol','resume'] + jobs

        sc = subprocess.run(cmd,stdout=subprocess.PIPE,
                         universal_newlines=True)
        #print(sc)
        if sc.returncode != 0:
            #print ( sc )
            logging.warning('Resume failed'+sc)
            return 1

if __name__ == '__main__':
    a = SlurmAction()
    a.reducePower(time.gmtime(),1)
    a.resume