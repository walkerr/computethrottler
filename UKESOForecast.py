import sys, time
import logging
import http.client
import ast
import requests
from urllib import parse

from BaseForecast import BaseForecast
from Config import config


class UKESOForecast(BaseForecast):
    def __init__(self):
        super().__init__(self)

        self.url = config.get("ukeso","url")
        self.threshold = int(config.get("ukeso","threshold")) # gCO2/kWh
        self.uhost, self.upath = self.splitUrl()

    def getForecast(self):
        # CO2 intensity UK. Also available per country and region.
#        sql_query =  '''SELECT * FROM  "0e5fde43-2de7-4fb4-833d-c7bca3b658b0" ORDER BY "_id" DESC LIMIT 100'''
        sql_query =  '''SELECT * FROM  "0e5fde43-2de7-4fb4-833d-c7bca3b658b0" WHERE forecast is not NULL ORDER BY "_id" DESC LIMIT 200'''
        params = {'sql': sql_query}
        try:
            response = requests.get('https://api.nationalgrideso.com/api/3/action/datastore_search_sql', params = parse.urlencode(params))
            data = response.json()["result"]
            print(data) # Printing data
        except requests.exceptions.RequestException as e:
            print(e.response.text)

        if 'records' in data.keys() :
            timeseries = data['records']

        logging.info("Got a time series of length: %d"%len(timeseries))
        # Convert timestamps to epoch seconds
        # [{'start':epoch, 'end':epoch}, 'value': float ]
        timevals = []
        # assume ordered
        for d in timeseries:
            timevals.append({'start': time.mktime(time.strptime(d['datetime'],'%Y-%m-%dT%H:%M:%S')),
                                'value':float(d['forecast'])})

        self.forecast = self.fillEnd(timevals)
        logging.info("Got a time series of length: %d"%len(self.forecast))


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    f = UKESOForecast()

    f.getForecast()

    print(f.forecast)
    f.scheduleNextAction()
   # UK CO2 intensity data does not have simple double peak structure
   # Need
    #f.nextMax()