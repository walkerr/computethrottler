#!/bin/bash
# To run as cron job on WNs. Reads a file on a shared FS and ensures governor in the required state.
#
# 1 file per host containing current governor
statusdir=/data/arc/session_ce3/computethrottler
# control file path
ctrl=/data/arc/session_ce3/computethrottler/ct.ctrl
# governor names for 2 states. Often default is schedutil.
powersave=powersave
resume=performance

# test overrides
statusdir=/tmp/ctstat ; ctrl=/root/ct.ctrl ; resume=schedutil

if [ ! -e $ctrl ] || [ ! -s $ctrl ] || [ ! -r $ctrl ];then
  echo "Control file not existing/filled/readable: "$ctrl 
 # Default to normal state
  command=resume
else
  command=`cat $ctrl`
fi

if [ $command = "powersave" ];then
  new=$powersave
elif [ $command = "resume" ];then
  new=$resume
elif [ $command = "null" ];then
 # Mechanism disabled. Stop here without even checking the current governor. 
  exit 0
else
  echo "Unrecognized command: "$command
  exit 1
fi

# only act current status different to ctrl file
current=`cpupower frequency-info  -p | gawk 'BEGIN{FS="\""}/governor/{print $2}'`

if [ $? -eq 0 ] && [ $current = $new ];then
  echo "Governor "$current" already in use. Command: "$command
  exit 0
fi 
# Change the governor
echo `date` $current" to "$new
cpupower frequency-set -g $new
current=`cpupower frequency-info  -p | gawk 'BEGIN{FS="\""}/governor/{print $2}'`
echo `date` $current > $statusdir/`hostname`.status

exit
