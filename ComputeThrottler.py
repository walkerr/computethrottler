# main loop to run as daemon
import sys
import time
import logging

from AwattarForecast import AwattarForecast
from EntsoeForecast import EntsoeForecast
from SignalForecast import SignalForecast

from SlurmAction import SlurmAction
from SSHAction import SSHAction
from Config import config

class ComputeThrottler:
    def __init__(self):
        logfile = config.get("logging","filename")
        logging.basicConfig(filename=logfile,level=logging.INFO,format='%(asctime)s %(message)s')
# Choose the forecaster subclass
        fsource = config.get('global','forecast').lower()
        logging.info("Forecast source configured as: %s"%fsource)
        if fsource == "entsoe":
            self.forecaster = EntsoeForecast()
        elif fsource == "awattar":
            self.forecaster = AwattarForecast()
        elif fsource == "signal":
            self.forecaster = SignalForecast()
        else:
            logging.warning("Forecast source not supported: %s"%fsource)

# Choose the action subclass
        atype = config.get('global','action').lower()
        logging.info("Action type configured as: %s"%atype)
        if atype == "slurm":
            self.action = SlurmAction()
        elif atype == "ssh":
            self.action = SSHAction()
        else:
            logging.error("Action type not supported: %s"%atype)
            sys.exit()

        self.debug = config.getboolean("global","debug")
        self.speedup = config.getint("global","speedup")
    def loop(self):
        while 1:
            self.run()

    def run(self):

        self.forecaster.getForecast()

        #pstart,pduration = f.scheduleNextAction()
#        pstart,pduration = self.forecaster.nextMax()
        pstart,pduration = self.forecaster.nextPause()

        if pstart and pduration>0:
            logging.info('Got a pause action start: %s for %d mins'%(time.ctime(pstart),pduration))
        # sleep until pause
        toSleep = max(pstart-time.time(),0)
        logging.info( 'Sleeping for %d mins'%int(toSleep/60) )
        if self.speedup > 1:
            logging.info("Accelerate by factor %d for debug"%self.speedup)
            toSleep /= self.speedup
        time.sleep(toSleep)
    # loop over list of multiple actions?
        self.action.reducePower(time.gmtime(),pduration)
        toSleep = pduration*60
        logging.info( 'Sleeping for %d mins'%int(toSleep/60) )
        if self.speedup > 1: toSleep /= self.speedup
        time.sleep(toSleep)
        self.action.resume()
        logging.info("Sleep for 3hrs to avoid flip-flop while still in peak")
        time.sleep(3*3600)

if __name__ == '__main__':
    ct = ComputeThrottler()
    ct.loop()
