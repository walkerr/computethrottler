import logging
import http.client
import ast

from BaseForecast import BaseForecast
from Config import config


class AwattarForecast(BaseForecast):
    def __init__(self):
        super().__init__(self)

        self.url = config.get("Awattar","url")
        self.uhost, self.upath = self.splitUrl()
        self.threshold = 500

    def getForecast(self):
        # Netto market price starting in the current hour.
        conn = http.client.HTTPSConnection(self.uhost)
        conn.request("GET", self.upath)
        r1 = conn.getresponse()
        dict = ast.literal_eval(r1.read().decode("utf-8"))
        if 'object' in dict and dict['object'] == 'list' and 'data' in dict:
            timeseries = dict['data']

        logging.info("Got a time series of length: %d"%len(timeseries))
        # Convert timestamps to epoch seconds
        # [{'start':time, 'end':time}, 'value': float ]
        timevals = []
        # assume ordered
        for d in timeseries:
            #  timevals.append({'start': time.gmtime(d['start_timestamp']/1000),
            #                    'end': time.gmtime(d['end_timestamp']/1000),'value':d['marketprice']})
            timevals.append({'start': int(d['start_timestamp'] / 1000), 'end': int(d['end_timestamp'] / 1000),
                             'value': d['marketprice']})
        self.forecast = timevals
    def nextPause(self):
        return self.nextMax()

if __name__ == '__main__':
    f = AwattarForecast()

    f.getForecast()

    print(f.forecast)
    #f.scheduleNextAction()
    f.nextMax()