import logging
import imaplib
import email
import re
import time

from BaseSignaller import BaseSignaller
from Config import config

class ImapSignaller(BaseSignaller):
    def __init__(self):
        super().__init__()
        self.host = config.get("signal","host")
        self.user = config.get("signal","user")
        self.password = config.get("signal","password")
        self.sender = config.get("signal","sender")
        self.subject = config.get("signal","subject")
        self.regexp = config.get("signal","regexp")
        self.reg2start_duration = config.get("signal","reg2start_duration")

    def connect(self):
        # connect to host using SSL
        self.imap = imaplib.IMAP4_SSL(self.host)
        # login to server
        logging.info("Connecting ...")
        self.imap.login(self.user, self.password)
        self.imap.select(mailbox='Inbox', readonly=True)

    def listen(self):
        logging.info("Waiting for a signal from %s@%s"%(self.user,self.host))
        self. connect()
        #self.imap.search(None,'(FROM "EON")')
        tmp, data = self.imap.search(None, 'FROM "%s" SUBJECT "%s"'%(self.sender,self.subject))
        for num in data[0].split():
            tmp, data = self.imap.fetch(num, '(RFC822)')
            print('Message: {0}'.format(num))
            msg = email.message_from_bytes(data[0][1])
            for part in msg.walk():
                if part.get_content_type() == 'text/plain':
                    body = part.get_payload()
                    break
        self.imap.close()
        # using the body from the last matching mail, i.e. newest, so correct by accident.
        return self.parseBody(body)

    def parseBody(self,body):
     # initialize start time is today then overwrite with email values
        start = time.gmtime()
        tFormat=''
        tStr=''
        #print(body,self.reg2start_duration)
        remat = re.search(self.regexp,body)
        if not remat:
            logging.warning("Pattern not matched: %s %s"%(self.regexp,body))
            return 0,0

        for v,t in zip(remat.groups(), self.reg2start_duration.split()):
            if t[0] == '%':
                tFormat += " " + t
                tStr += " " + v
            elif t == 'duration':
                duration = int(v)
            elif t == 'durationunits':
                durationUnits = v
            else:
                logging.warning("Unknown value in reg2start_duration: %s"%t)
        # might need to handle incomplete date info from email
        # default is year 1900 and not helpful
        starttime = time.strptime(tStr,tFormat)

        # deal with duration units, or assume in mins
        # handle hrs as float?
        if durationUnits in ['hrs','hours','h']:
            duration *= 60

        return int(time.mktime(starttime)),duration

if __name__ == '__main__':
    sig = ImapSignaller()
    sig.user='anyone'
    # only when running in terminal (e.g.not PyCharm)
    import getpass
    sig.password = getpass.getpass("Entering password: ")

    st,d = sig.listen()
    print (st,d)